﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace JyEditor
{

    public class JyEditorMenuItem
    {
        [MenuItem("JyEditor/AssetBundle/Build Android AssetBundle")]
        public static void BuildAndroidAssetBundle()
        {
            BuildAssetBundleEditor.Ins.BuildAndroidAssetBundle();
        }

        [MenuItem("JyEditor/AssetBundle/Build Iphone AssetBundle")]
        public static void BuildIphoneAssetBundle()
        {
            BuildAssetBundleEditor.Ins.BuildIphoneAssetBundle();
        }

        [MenuItem("JyEditor/AssetBundle/Build Windows AssetBundle")]
        public static void BuileWindowsAssetBundle()
        {
            BuildAssetBundleEditor.Ins.BuildWindowsAssetBundle();
        }

        [MenuItem("JyEditor/AssetBundle/Clear Unused AssetBundle")]
        public static void ClearUnusedAssetBundle()
        {
            BuildAssetBundleEditor.Ins.ClearUnusedAssetBundle();
        }

        [MenuItem("JyEditor/AssetBundle/Clear Unused AssetBundle Names")]
        public static void ClearUnusedAssetBundleNames()
        {
            BuildAssetBundleEditor.Ins.ClearUnusedAssetbundleNames();
        }

        [MenuItem("JyEditor/AssetBundle/Clear All AssetBundle Names")]
        public static void ClearAllAssetBundleNames()
        {
            BuildAssetBundleEditor.Ins.ClearAllAssetbundleNames();
        }

        [MenuItem("JyEditor/TestReadFiles")]
        public static void TestReadFiles()
        {
            List<string> files = new List<string>();
            List<string> paths = new List<string>();
            string metaPath = Application.dataPath + "/_resource/";
            var bas = BuildAssetBundleEditor.Ins;
            FileHelper.Ins.ReadFile(metaPath, ref files, ref paths);

            Dictionary<string, JyFileInfo> temp = new Dictionary<string, JyFileInfo>();
            foreach (var file in files)
            {
                JyFileInfo jyFile = new JyFileInfo(file);
                temp.Add(jyFile.Name, jyFile);
            }

            FileHelper.Ins.GenerateFileText(temp, PathHelper.Ins.FileTextPath);
            AssetDatabase.Refresh();

            Debug.Log("-------------");
        }
    }

}
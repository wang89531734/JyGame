﻿/*
 *  date: 2018-08-19
 *  author: John-chen
 *  cn: UIRoot的信息
 *  en: todo:
 */

using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// UIroot的基础信息
    /// NGUI UGUI可分别继承后实现自己的逻辑
    /// </summary>
    public class UIRootInfo : IUIInfoSetting
    {
        /// <summary>
        /// root节点对象
        /// </summary>
        public GameObject RootGameObjct { get { return _rootObj; } }

        /// <summary>
        /// root节点transform
        /// </summary>
        public Transform RootTransform { get { return _rootTran;} }

        /// <summary>
        /// UI相机
        /// </summary>
        public Camera UICamera { get { return _uiCamera;} }

        /// <summary>
        /// 用uiroot节点构造
        /// </summary>
        /// <param name="rootObj"></param>
        public UIRootInfo(GameObject rootObj, VoidDelegateSetWindow setWindowFunc)
        {
            _rootObj = rootObj;
            _rootTran = rootObj.transform;

            _setWindowFunc = setWindowFunc;
        }

        /// <summary>
        /// 设置window的信息,不用框架下,不同设置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="window"></param>
        public virtual void SetWindow<T>(T window) where T : BaseWindow
        {
            _setWindowFunc?.Invoke(window);
        }
        
        protected Camera _uiCamera;
        protected GameObject _rootObj;
        protected Transform _rootTran;

        protected VoidDelegateSetWindow _setWindowFunc;

    }

    public delegate void VoidDelegateSetWindow(BaseWindow wnd);

    /// <summary>
    /// ui信息设置接口
    /// </summary>
    public interface IUIInfoSetting
    {
        /// <summary>
        /// 设置UI信息,包括层级等
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="window"></param>
        void SetWindow<T>(T window) where T : BaseWindow;
    }
}
﻿/*
 *  date: 2018-03-27
 *  author: John-chen
 *  cn: 对象管理模块
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 对象管理模块,包括对象池
    /// </summary>
    public class ObjectModule : BaseModule
    {
        public ObjectModule(string name = ModuleName.Object) : base(name)
        {
        }

        public override void InitGame()
        {
            
        }
    }
}

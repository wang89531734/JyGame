﻿/*
 *  date: 2018-04-21
 *  author: John-chen
 *  cn: 游戏基础模块
 *  en: todo:
 */

using UnityEngine;
using System.Collections.Generic;

namespace JyFramework
{
    /// <summary>
    /// 游戏基础模块
    /// </summary>
    public class BaseModule : IGameLife, IModule
    {
        /// <summary>
        /// 当前模块名
        /// </summary>
        public string Name { get { return _name; } }

        /// <summary>
        /// 游戏初始化
        /// 时机： 所有模块添加完毕后执行
        /// </summary>
        public virtual void InitGame()
        {
            Debug.Log(_name + ".InitGame();");
        }

        /// <summary>
        /// 游戏开始
        /// 时机： 基于 MonoBehaviour 的 Start 中执行
        /// </summary>
        public virtual void StartGame()
        {
            Debug.Log(_name + ".StartGame();");
        }

        /// <summary>
        /// 游戏更新
        /// 时机： 基于 MonoBehaviour 的 Update 中执行
        /// </summary>
        /// <param name="deltaTime"></param>
        public virtual void UpdateGame(float deltaTime)
        {
            //Debug.Log("BasemModule update!");
        }

        /// <summary>
        /// 游戏暂停
        /// </summary>
        public virtual void PauseGame()
        {
            Debug.Log(_name + ".PauseGame();");
        }

        /// <summary>
        /// 退出游戏
        /// </summary>
        public virtual void ExitGame()
        {
            Debug.Log(_name + ".ExitGame();");
        }

        /// <summary>
        /// 添加本模块Manager
        /// </summary>
        /// <param name="mgr"></param>
        public virtual void AddManager(BaseManager mgr)
        {
            if (mgr == null || _mgrs.Contains(mgr)) return;
            _mgrs.Add(mgr);
            mgr.Init();
        }

        /// <summary>
        /// 移除本模块Manager
        /// </summary>
        /// <param name="mgr"></param>
        public virtual void RemoveManager(BaseManager mgr)
        {
            if (mgr == null) return;
            if (!_mgrs.Contains(mgr))
            {
                mgr.Remove();
                return;
            }

            _mgrs.Remove(mgr);
            mgr.Remove();
        }

        /// <summary>
        /// 添加控制器
        /// </summary>
        /// <param name="ctrl"></param>
        public virtual void AddController(BaseController ctrl)
        {
            if (ctrl == null || _ctrls.Contains(ctrl)) return;
            _ctrls.Add(ctrl);
            ctrl.Init();
        }

        /// <summary>
        /// 移除控制器
        /// </summary>
        /// <param name="ctrl"></param>
        public virtual void RemoveController(BaseController ctrl)
        {
            if (ctrl == null) return;
            if(!_ctrls.Contains(ctrl))
            {
                ctrl.Remove();
                return;
            }
            _ctrls.Remove(ctrl);
            ctrl.Remove();
        }

        /// <summary>
        /// 结束清理自身
        /// </summary>
        public virtual void ClearSelf()
        {
            Debug.Log(_name + ".ClearSelf();");
            _mgrs.Clear();
            _ctrls.Clear();
            _mgrs = null;
            _ctrls = null;
        }

        protected string _name;
        protected List<BaseManager> _mgrs;
        protected List<BaseController> _ctrls;

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="ec"> BaseModule 名字 </param>
        protected BaseModule(string name = "BaseModule")
        {
            _name = name;
            _mgrs = new List<BaseManager>();
            _ctrls = new List<BaseController>();
            Debug.Log(name + ".ctor();");
        }
    }

}